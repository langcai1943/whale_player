/**
 * list group and free, data put list and free list
 */

#ifndef _SYS_LIST_H
#define _SYS_LIST_H

#include <errno.h>
#include <stddef.h>
#include "list.h"

struct slists_group
{
    struct list_head group, free;
};
typedef struct slists_group slists_group_t, *pslists_group_t;

struct slists
{
    struct list_head node;
    pslists_group_t group;
};
typedef struct slists slists_t, *pslists_t;

void slists_group_init(pslists_group_t psg);
void slists_group_deinit(pslists_group_t psg);
void slists_init(pslists_t psl);
int slists_free(pslists_t psl);
int slists_put(pslists_t  psl);
pslists_t slists_alloc(pslists_group_t psg);
pslists_t slists_get(pslists_group_t psg);
int slists_topnum(pslists_group_t psg);

#endif // _SYS_LIST_H
