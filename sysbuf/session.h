#ifndef _SESSION_H_
#define _SESSION_H_

#include <stdio.h>
#include <string.h>
/**
 * Session buffer interface
 */

#include <unistd.h>
#include "sysbuf.h"

////////////////////////////////////////////////////////////////////////////////
typedef struct
{
	slists_t list;
	psysbuf_t buf;
} session_buffer_t;

struct SESSION
{
	// put another session varibals in this
	slists_group_t buffer_group;
};

extern void SessionInitAll(void);
extern void SessionBufferInit(struct SESSION *session);
extern session_buffer_t *SessionBufferPush(struct SESSION *session, psysbuf_t buf);
extern psysbuf_t SessionBufferPop(struct SESSION *session);
extern int SessionBufferTopNum(struct SESSION *session);
extern void SessionBufferDeInit(struct SESSION *session);

#endif //_SESSION_H_