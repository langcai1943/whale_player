/**
 * buffer pointer and list
 */

#ifndef _SYS_BUFFER_H
#define _SYS_BUFFER_H

#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include "syslist.h"

#define SYS_BUF_MAX_COUNT       196
#define SYS_BUF_MARK            0xbead
#define SYS_BUF_FLAG_MARK       (0xFF00)
#define SYS_BUF_FLAG_EOS        (0x1000)
#define SYS_BUF_FLAG_EOF        (0x8000)

/* data buffer */
#define DATBUF_MAX_BUF_CNT 20
#define DATBUF_MAX_BUF_LEN 0x80000
/* message buffer */
#define MSGBUF_MAX_BUF_CNT 32
#define MSGBUF_MAX_BUF_LEN 0x40000

typedef unsigned long phys_addr_t;

enum
{
    SYSBUF_GROUPTYPE_SRAM = 0,
    SYSBUF_GROUPTYPE_DDR,
};

typedef struct
{
    uint16_t type;
    uint16_t count;
    uint32_t unitsize;
    phys_addr_t vaddr;
    phys_addr_t haddr;
} sysbuf_group_t;

typedef struct
{
    uint32_t nLowPart;
    uint32_t nHighPart;
} sysbuf_user_t;

struct sysbuf
{
    struct slists list;
    uint16_t mark;
    uint16_t flags;
    phys_addr_t vaddr;
    phys_addr_t haddr;
    int32_t maxsize;
    int32_t offset;
    int32_t size;
    uint32_t buffid;
    sysbuf_user_t user;
};
typedef struct sysbuf sysbuf_t, *psysbuf_t;

/* sys_buf_t::group */
enum
{
    SYSBUF_GROUP_FREE = 0,
    SYSBUF_GROUP_MSGBUFS,
    SYSBUF_GROUP_DATBUFS,
    SYSBUF_GROUP_COUNT,
};

void sysbuf_init(void);
int sysbuf_group_reset(sysbuf_group_t *gd, int group);
int sysbuf_free(psysbuf_t  buf);
psysbuf_t sysbuf_alloc(int group);
void sysbuf_reset(int group);

#endif /* _SYS_BUFFER_H */
