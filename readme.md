# 模块统一接口，缓存队列、FIFO，双向循环链表模块，移植Linux menuconfig模块配置自己的应用程序。  

|作者|将狼才鲸|
|---|---|
|创建日期|2021|

* 配套哔哩哔哩视频：  
[【C语言】用Linux风格编译你自己的程序](https://www.bilibili.com/video/BV1rA411K7tr/)  
[【C语言】用结构体和函数指针实现面向对象](https://www.bilibili.com/video/BV19K4y1K7SS/)  
[【C语言】实现模块消息队列和数据队列](https://www.bilibili.com/video/BV1364y1Q7mV/)  

---

* 编译方式：  
## 1. Get the 'menuconfig' source code of linux  
Get scripts folder(for make menuconfig function) of linux kernel stable source code v5.10.12 on 20210130.  
[linux-5.10.12](https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.10.12.tar.xz)  
[网易开源镜像站](http://mirrors.163.com/kernel/v5.x/linux-5.10.12.tar.gz)  

---

## 2. Modify scripts folder of linux  
Modify Kbuild and Kconfig  
name@pc:~$ make menuconfig  

---

## 3. Add user codes  
make menuconfig  
make  

---

# 4. Run application  
./YourProgramName  
