#! /usr/bin/python
#/*
# This file is free ; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program;
#*/

import sys

linux_vmem = open(sys.argv[1], "r")
linux_r    = linux_vmem.read()
linux_list = linux_r.split('\n')
linux_list += ['00000000', '00000000', '00000000', '00000000', '00000000', '00000000', '00000000']
linux_list.remove('')

print('@20000')

for i in range(0, int(len(linux_list) / 8)):
    pre = linux_list[i * 8: i * 8 + 8]

    index  = 4
    target = ""
    for j in range(0, 2):
        target = target + pre[6][index:index + 2]
        target = target + pre[4][index:index + 2]
        target = target + pre[2][index:index + 2]
        target = target + pre[0][index:index + 2]
        index = index + 2
    print(target)

    index  = 0
    target = ""
    for j in range(0, 2):
        target = target + pre[6][index:index + 2]
        target = target + pre[4][index:index + 2]
        target = target + pre[2][index:index + 2]
        target = target + pre[0][index:index + 2]
        index = index + 2
    print(target)

    index  = 4
    target = ""
    for j in range(0, 2):
        target = target + pre[7][index:index + 2]
        target = target + pre[5][index:index + 2]
        target = target + pre[3][index:index + 2]
        target = target + pre[1][index:index + 2]
        index = index + 2
    print(target)

    index  = 0
    target = ""
    for j in range(0, 2):
        target = target + pre[7][index:index + 2]
        target = target + pre[5][index:index + 2]
        target = target + pre[3][index:index + 2]
        target = target + pre[1][index:index + 2]
        index = index + 2
    print(target)


